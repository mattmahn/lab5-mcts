package longatoj_mahnkem;

import buswrapper.RealtimeWrapperException;

import javax.swing.*;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		
		try {
			ConcreteSubject concreteSubject = ConcreteSubject.getInstance();
			GUI test = GUI.getInstance();
			concreteSubject.begin();
			
			BigController big = new BigController();
			Thread.sleep(3000);//Sleep for a little bit to let the subject get updates so GUI can function.
			test.setbusList(big.busesToDisplay());

			test.addModifier(big);
			test.setVisible(true);
		} catch (RealtimeWrapperException e) {
			JOptionPane.showMessageDialog(null,
			                              "Something bad happened when accessing the RideMCTS API:\n\n" +
			                              e.getMessage());
			e.printStackTrace();
		}

//		TextController test = new TextController();
//		test.update(new State(new VehicleText("1", "20150112 12:55", "1", "56")));
	}
}

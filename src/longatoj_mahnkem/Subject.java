package longatoj_mahnkem;

/**
 * This class collects information for {@link longatoj_mahnkem.Watcher watchers} and notifies them of any new, updated
 * data.
 * <p>
 * A part of the observer pattern.
 *
 * @author Jordan Longato
 * @author Matthew Mahnke
 */
public interface Subject {
	/**
	 * Adds the specified {@link longatoj_mahnkem.Watcher} to a list of watchers to notify
	 *
	 * @param o the Watcher to add
	 */
	public void attach(Watcher o);

	/**
	 * Notifies all watchers of an updated state
	 */
	public void notifyWatchers();
}

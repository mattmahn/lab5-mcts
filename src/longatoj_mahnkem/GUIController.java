package longatoj_mahnkem;

import java.util.List;

import longatoj_graphingpanel.GraphingPanel;

/**
 * A GuiController that maintains the functions of the GUI and draws apropriate
 * points based on what BigController sends to this class.
 * <p>
 * 
 * @author longatoj
 *
 */
public class GUIController {
	/*
	 * GraphingPanel to draw points to.
	 */
	private GraphingPanel grapher;
	/*
	 * A reference point for where city hall is located
	 */
	private double cityLat = 43.04173;
	/*
	 * A reference point for where city hall is located
	 */
	private double cityLong = -87.90976;

	/**
	 * Creates a GUIControlelr with a GraphingPanel as the JPanel to draw states
	 * information to.
	 * 
	 * @param grapher
	 *            The graphingPanel that this maintains and controls
	 */
	public GUIController(GraphingPanel grapher) {
		this.grapher = grapher;
	}

	/**
	 * Calls to this method will invoke a clear area on the GraphingPanel that
	 * this is associated with and will cause all States passed to be displayed
	 * on the GraphingPanel
	 * 
	 * @param selectedStates
	 */
	public void update(List<State> selectedStates) {
		grapher.clearArea();
		for (State state : selectedStates) {
			drawPoint(state);
		}

		grapher.createLabeledPoint(grapher.getWidth()/2, grapher.getHeight()/2, "City Hall");
	}

	/**
	 * Draws a state onto the GraphingPanel
	 * 
	 * @param cur
	 *            State to draw to the GUI
	 */
	public void drawPoint(State cur) {
		double statesLatitude = cur.getLatitude();
		double statesLongitude = cur.getLongitude();
		double differenceX = -1 * (cityLong - statesLongitude);
		double differenceY = (cityLat - statesLatitude);
		int scaledY = 0, scaledX = 0;
		scaledX = (int) (grapher.getWidth() / 2 + Math
				.round(GraphingPanel.GRID_SCALE * (differenceX / .0144928)));
		scaledY = (int) (grapher.getHeight() / 2 + Math
				.round(GraphingPanel.GRID_SCALE * (differenceY / .0144928)));

		grapher.createLabeledPoint(scaledX, scaledY,
				String.valueOf(cur.getId()));

	}
}

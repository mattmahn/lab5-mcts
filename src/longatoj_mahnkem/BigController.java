package longatoj_mahnkem;

import buswrapper.RealtimeWrapperException;

import java.util.*;
/** 
 * A super controller that maintains other controllers.
 * <p> The purpose of this class is to delegate tasks to other class's/controllers.
 * Like TextController and GUIController. Calls to those other controllers will determine
 * what this class will do.
 * <p>
 * In order for this class to maintain the latest updated information on the states
 * a call to update() must be made every time new information is taken in by the subject.
 * <p>
 * We use a push model for this observer pattern.
 * @author longatoj
 *
 */
public class BigController implements Watcher {
	/*
	 * A map whose purpose is to related State ID's to a text controller
	 */
	private Map<Long, TextController> txtMap;
	/*
	 * A single GUIController that will determine what to do with the GUI when an
	 * event has occurred.
	 */
	private GUIController guiController;
	/*
	 * A list of the selected Items in the GUI to pass to the guiController when
	 * it is necessary.
	 */
	private List<String> selectedItems;
	/*
	 * A map containing the most up to date states. This map should be updated regularly when this 
	 * class's update() is called
	 */
	private Map<Long, State> updatedStates;
	/*
	 * A reference to the gui to perform operations such as changing the text and changing the grid scale 
	 * if necessary. 
	 */
	private GUI gui;
	public BigController(){
		txtMap = new HashMap<Long, TextController>();
		selectedItems = new LinkedList<String>();
		gui = GUI.getInstance();
		guiController = new GUIController(gui.getGrapher());
		updatedStates = new HashMap<Long, State>();
		try {
			Subject sub = ConcreteSubject.getInstance();
			sub.attach(this);
		} catch (RealtimeWrapperException e) {
			e.printStackTrace();
		}
		
		
	}
	

	/**
	 * Updates the state information for the state passed.s
	 */
	@Override
	public synchronized void update(State state) {
		if(state != null){
			checkMaps(state);
			updatedStates.put(state.getId(), state);
			txtMap.get(state.getId()).update(state);
			updateText();
			guiController.update(getSelectedStates());

			gui.repaint();
		}
	}
	/**
	 * Determines the amount of buses to display. If this list is empty the method will keep running
	 * until there is something to display. 
	 * <p>
	 * 
	 * @return Array of Strings that contain the ID of each bus to display
	 */
	public String[] busesToDisplay() {
		String[] keys;
		do{
		keys = new String[updatedStates.size()];
		Set<Long> keySet = updatedStates.keySet();
		Iterator<Long> it = keySet.iterator();
		for(int i = 0; i<updatedStates.size(); i++)
			keys[i] = String.valueOf(it.next());
		}while(keys.length == 0);
		return keys;
	}


	/*
	 * Will make adjustments to the graphical part of the GUI if necessary.
	 */
	private void updateGUI() {
		guiController.update(getSelectedStates());
		
	}
	/*
	 * Returns a list of states that are selected from the JList in the gui.
	 */
	private List<State> getSelectedStates() {
		List<State> selectedStates = new LinkedList<State>();
		for(String stringID : selectedItems){
			selectedStates.add(updatedStates.get(Long.parseLong(stringID)));
		}
		return selectedStates;
		
	}
	/*
	 * Checks to see if the maps maintained by this class are up to date with the state
	 * that is passed in. If they are not then this map will put the newest state in.
	 */
	private void checkMaps(State state) {
		if(!txtMap.containsKey(state.getId())){
			//new controller must be created
			txtMap.put(state.getId(), new TextController(state));
		}
		if(!updatedStates.containsKey(state.getId())){
			updatedStates.put(state.getId(), state);
		}
		
		
	}
	/**
	 * Call this method when a change has occurred in the GUI that is
	 * attached to this controller.
	 * @param a TODO
	 */
	public void notifyChange(List<String> a){
		selectedItems = a;
		updateText();
		updateGUI();
		
	}

	/*
	 * Updates the text area of the GUI by setting the area to an empty string then to 
	 * a description of each of the states that this class maintains
	 */
	private void updateText() {
		gui.setDescription("");
		String fullDescription = "";
		for(String item: selectedItems){
			long ID = Long.parseLong(item);
			TextController txtController = txtMap.get(ID);
			fullDescription += txtController.toString() +"\n";
		}

		gui.setDescription(fullDescription);
		if(selectedItems.size() == 0){
			gui.getGrapher().clearArea();
		}
		
	}
	
}

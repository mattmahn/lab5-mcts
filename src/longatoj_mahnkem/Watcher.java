package longatoj_mahnkem;

/**
 * This class is notified via the <tt>update</tt> method when {@link longatoj_mahnkem.Subject} gets new information.
 * <p>
 * A part of the observer pattern.
 *
 * @author Jordan Longatio
 * @author Matthew Mahnke
 */
public interface Watcher {
	/**
	 * Called by the Subject when there is new data
	 *
	 * @param state the new data
	 */
	public void update(State state);

}

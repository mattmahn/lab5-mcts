package longatoj_mahnkem;

import buswrapper.VehicleText;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class represents the state of a {@link buswrapper.VehicleText vehicle}. The attributes of VehicleText have been
 * mapped into their respective data types: </p>
 * <table>
 *     <tr><td>VehicleText Attribute</td> <td>Data Type in State</td></tr>
 *     <tr><td>lat</td> <td>double</td></tr>
 *     <tr><td>lon</td> <td>double</td></tr>
 *     <tr><td>vid</td> <td>long</td></tr>
 *     <tr><td>tmstmp</td> <td>Date</td></tr>
 * </table>
 *
 * @author Matthew Mahnke
 */
public class State {
	/**
	 * The last latitude of the vehicle
	 */
	private final double latitude;
	/**
	 * The last longitude of the vehicle
	 */
	private final double longitude;
	/**
	 * The identification number of the vehicle
	 */
	private final long id;
	/**
	 * The timestamp of when this state is accurate
	 */
	private final Date timestamp;

	/**
	 * Creates a State based on the specified {@link buswrapper.VehicleText}.
	 * <p>
	 * The attributes of vehicleText are parsed into friendlier formats such as doubles and Dates
	 *
	 * @param vehicleText the
	 */
	public State(VehicleText vehicleText) {
		this.latitude = Double.parseDouble(vehicleText.lat);
		this.longitude = Double.parseDouble(vehicleText.lon);
		this.id = Long.parseLong(vehicleText.vid);

		Date tempTimeStamp;
		try {
			DateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm");
			tempTimeStamp = df.parse(vehicleText.tmstmp);
		} catch (ParseException e) {
			System.err.println("Could not parse the timestamp. Setting timestamp to current time.");
			tempTimeStamp = new Date();
		}
		this.timestamp = tempTimeStamp;
	}

	/**
	 * Returns the latitude of this State
	 *
	 * @return the latitude of this State
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Return the longitude of this State
	 *
	 * @return the longitude of this State
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Returns the ID of this State
	 *
	 * @return the ID of this State
	 */
	public long getId() {
		return id;
	}

	/**
	 * Returns the timestamp of this State
	 *
	 * @return the timestamp of this State
	 */
	public Date getTimestamp() {
		return timestamp;
	}
}

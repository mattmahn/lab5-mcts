package longatoj_mahnkem;

import buswrapper.RealtimeWrapper;
import buswrapper.RealtimeWrapperException;
import buswrapper.VehicleText;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.Timer;

/**
 * This class gathers data about the Green line MCTS buses and notifies {@link longatoj_mahnkem.Watcher watchers} about
 * new updates of the buses.
 *
 * @author Matthew Mahnke
 */
public class ConcreteSubject implements Subject {
	/**
	 * Allows the retrieval of bus data from the RideMCTS API to be run in a separate thread
	 */
	private class FetchVehicleTask implements Runnable {
		/**
		 * When an object implementing interface <code>Runnable</code> is used to create a thread, starting the thread
		 * causes the object's <code>run</code> method to be called in that separately executing thread.
		 * <p>
		 * The general contract of the method <code>run</code> is that it may take any action whatsoever.
		 *
		 * @see Thread#run()
		 */
		@Override
		public void run() {
			try {
				List<VehicleText> vehicleTexts = realtimeWrapper.fetchVehicles();
				busStates.clear();
				makeStates(vehicleTexts);
			} catch (RealtimeWrapperException e) {
				System.err.println("Something bad happened while accessing the Ride MCTS API:");
				e.printStackTrace();
			}
		}

		/**
		 * Creates states from {@code vehicleTextList} and adds them to {@link longatoj_mahnkem.ConcreteSubject#busStates}
		 *
		 * @param vehicleTextList the Collection of VehicleTexts to add
		 */
		private void makeStates(Collection<VehicleText> vehicleTextList) {
			vehicleTextList.forEach(vt -> busStates.add(new State(vt)));
		}
	}

	/**
	 * How often the program should request new data from the MCTS API.
	 * <p>
	 * Currently set to update every 20 seconds.
	 */
	public static final long API_REQUEST_INTERVAL = (long) ((60 / 3) * 1E3);
	/**
	 * Singleton reference for this class
	 */
	private static volatile ConcreteSubject singleton;

	/**
	 * Stores the states of each bus
	 */
	private Set<State> busStates;
	/**
	 * The Runnable which allows getting bus information in separate threads
	 */
	private FetchVehicleTask fetchVehicleTask;
	/**
	 * Gets information from the RideMCTS API
	 */
	private RealtimeWrapper realtimeWrapper;
	/**
	 * A set of watchers that will be notified of changes
	 */
	private Set<Watcher> watchers;

	/**
	 * Creates a new ConcreteSubject with the specified key file pathname
	 *
	 * @param apiKeyPath the path of the key file
	 * @throws RealtimeWrapperException
	 */
	private ConcreteSubject(String apiKeyPath) throws RealtimeWrapperException {
		this.busStates = new HashSet<>();
		this.watchers = new HashSet<>();
		this.realtimeWrapper = new RealtimeWrapper(apiKeyPath);
		this.fetchVehicleTask = new FetchVehicleTask();
	}

	/**
	 * Returns a singleton instance of this class using the default API key file path of <tt>key.txt</tt>
	 *
	 * @return a singleton instance of this class
	 * @throws RealtimeWrapperException
	 */
	public static ConcreteSubject getInstance() throws RealtimeWrapperException {
		return getInstance("key.txt");
	}

	/**
	 * Returns a singleton instance of this class using the specified path to the API key file
	 *
	 * @param apiKeyPath the path of the key file
	 * @return a singleton instance of this class
	 * @throws RealtimeWrapperException
	 */
	private static ConcreteSubject getInstance(String apiKeyPath) throws RealtimeWrapperException {
		if (singleton == null) {
			synchronized (ConcreteSubject.class) {
				if (singleton == null) {
					singleton = new ConcreteSubject(apiKeyPath);
				}
			}
		}
		return singleton;
	}

	/**
	 * Starts a timer that will run {@link longatoj_mahnkem.ConcreteSubject.FetchVehicleTask} using {@code
	 * SwingUtilities.invokeAndWait} every <tt>API_REQUEST_INTERVAL</tt> seconds.
	 * <p>
	 * NOTE: This probably shouldn't be called before a Swing object has been made.
	 */
	public void begin() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					SwingUtilities.invokeAndWait(fetchVehicleTask);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					System.err.println("An exception has been thrown from longatoj_mahnkem.FetchVehicleTask");
					e.printStackTrace();
				}
				notifyWatchers();
			}
		}, 0, API_REQUEST_INTERVAL);
	}

	/**
	 * Adds the specified {@link longatoj_mahnkem.Watcher} to {@link longatoj_mahnkem.ConcreteSubject#watchers}
	 *
	 * @param o the Watcher to add
	 */
	@Override
	public void attach(Watcher o) {
		watchers.add(o);
	}

	/**
	 * Notifies all watchers of an updated state
	 */
	@Override
	public void notifyWatchers() {
		for (Watcher watcher : watchers) {
			busStates.forEach(watcher::update);
		}
	}

	/**
	 * Returns the Set of States for each bus
	 *
	 * @return the Set of States for each bus
	 */
	public Set<State> getBusStates() {
		return busStates;
	}

	/**
	 * Returns the Set of Watchers that get notified
	 *
	 * @return the Set of Watcher that get notified
	 */
	public Set<Watcher> getWatchers() {
		return watchers;
	}
}

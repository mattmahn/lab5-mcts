package longatoj_mahnkem;

import java.util.Date;
/**
 * A TextController that maintains the information of one State. This class is able to 
 * remember previous version of the state and calculate averages based on the previous state.
 * <p>
 * If another class needs data on the state this controller maintains a call to toString()
 * should be sufficient.
 * @author longatoj
 *
 */
public class TextController implements Watcher {
	/*
	 * The previous state that was associated with this TextController
	 */
	private State previousState;
	/*
	 * ID of the state that this maintains.
	 */
	private final long ID;
	/*
	 * The distance and velocity of the state
	 */
	private double distance,velocity;
	/**
	 * Creates a TextController for the state and updates the information of this state.
	 * @param initialState Initial state that this maintains.
	 */
	public TextController(State initialState){
		ID = initialState.getId();
		this.update(initialState);
	}
	/**
	 * Calls to this method will cause this class to update the information it stores for the 
	 * state that is passed. If this TextController contains no previous data on the state passed
	 * it will set previous data to the current data.
	 */
	public void update(State state) {
		if(previousState == null){
			previousState = state;
		}
		distance = calcDistance(state.getLongitude(), state.getLatitude());
		double distanceChanged = calcDistance(state.getLongitude(), state.getLatitude(), previousState.getLongitude(), previousState.getLatitude());
		double timeChanged = calcTimeChanged(state.getTimestamp(), previousState.getTimestamp());
		velocity = calcVelocity(distanceChanged, timeChanged);
		previousState = state;
		
	    
	}
	/*
	 * Calculates the time difference between the previous state and the current state of the object
	 */
	private double calcTimeChanged(Date end, Date initial) {
		double seconds = (end.getTime() - initial.getTime())/1000;
		double minutes = seconds/60;
		double hours = minutes/60;
		return hours;
	}
	/*
	 * Calculates the velocity that the state has.
	 */
	private double calcVelocity(double distance, double time) {
		if(time == 0){
			time = 1; //avoid initial condition of time = 0
		}
		return distance/time;
	}
	/*
	 * Calculates the distance between two longitude,latitude points.
	 */
	private double calcDistance(double longA, double latA, double longB, double latB){
		
		double earthRadius = 3958.75;//miles
	    double dLat = Math.toRadians(latB-latA);
	    double dLng = Math.toRadians(longB-longA);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(latA)) * Math.cos(Math.toRadians(latB));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    
	    return dist;//miles
	}
	/*
	 * Calculates the distance between a longitude, latitude and city hall 
	 * which is located at 43.04173 north, -87.90976 east
	 */
	private double calcDistance(double longitude, double latitude) {
		longitude = Math.abs(longitude);
		latitude = Math.abs(latitude);
		double latA = latitude;
		double latB = 43.04173;
		double longA = longitude;
		double longB = 87.90976;
		double earthRadius = 3958.75;
	    double dLat = Math.toRadians(latB-latA);
	    double dLng = Math.toRadians(longB-longA);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(latA)) * Math.cos(Math.toRadians(latB));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    return dist;
		
	}
	/**
	 * Returns the string form of the State that this text controller maintains.
	 */
	public String toString(){
		return "ID: " + ID + " Approximate distance from city hall: " + Math.round(distance)+ " miles, " + " Velocity: " + Math.round(velocity) + "m/s";
	}
}

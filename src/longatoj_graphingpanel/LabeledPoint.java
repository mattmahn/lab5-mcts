package longatoj_graphingpanel;

/**
 * A class that maintains information on a LabeledPoint. This class is used by the 
 * graphing panel.
 * @author longatoj
 *
 */
public class LabeledPoint {
	private int x, y;
	private String label;

	public LabeledPoint(int x, int y, String label) {
		this.x = x;
		this.y = y;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}

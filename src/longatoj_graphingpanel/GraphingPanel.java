package longatoj_graphingpanel;

import javax.swing.*;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.RoundRectangle2D;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
/**
 * A JPanel that allows for custom lines and points to be made in its area.
 * <p>
 * When using this JPanel, in order for things to be drawn to the main JFrame,
 * a call to the JFrame's repaint() must be made in order for this JPanel to update itself
 * 
 * @author longatoj
 *
 */
public class GraphingPanel extends JPanel {
	/**
	 * Sets the scale of the grid. This number will determine how often the lines
	 * of the grid are drawn. Lower numbers make the grid draw more often and visa versa.
	 */
	public static final int GRID_SCALE = 30;
	/*
	 * Something that the Java people like to tell me to put in for some reason I don't understand
	 */
	private static final long serialVersionUID = -6646603733462067295L;
	/*
	 * List that contains the shapes to draw to this JPanel
	 */
	private List<Shape> thingsToDraw;
	/*
	 * List that contains the labeledPoints to draw to this JPanel
	 */
	private List<LabeledPoint> labeledPoints;
	/*
	 * Variables that will change if a user would like the color of the pens changed.
	 */
	private Color backgroundColor,penColor;
	/**
	 * Creates a GraphingPanel that by default contains no shapes, the pen color is green
	 * and the background color is a charcoal black. 
	 * <p>See methods within this class in order to add shapes and change pen color/ background 
	 * color. 
	 */
	public GraphingPanel(){
		thingsToDraw = new LinkedList<Shape>();
		labeledPoints = new LinkedList<LabeledPoint>();
		backgroundColor = new Color(45,45,45);
		labeledPoints = Collections.synchronizedList(labeledPoints);
		penColor = Color.green;
	}
	/**
	 * Method that paints things to the JPanel. This method is the main reason things will
	 * show up in the JPanel. This method uses class variables and iterates through them and
	 * uses the Graphics2D object to draw them to the panel.
	 */
	@Override
	protected synchronized void paintComponent(Graphics g) {
		Graphics2D gr = (Graphics2D) g;
		gr.setColor(backgroundColor);
		gr.fillRect(0, 0, this.getWidth(), this.getHeight());
		gr.setColor(new Color(57, 58, 55));
		createGrid(gr);
		gr.setColor(penColor);
		thingsToDraw.forEach(e -> gr.draw(e));
		labeledPoints.forEach(point -> paintLabeledPoint(g,point));
		
		
	}
	/*
	 * Creates the grid which is a bunch of lines at an interval determined by the 
	 * class constant.
	 */
	private void createGrid(Graphics2D gr) {
		
		for (int x = 0; x < this.getWidth(); x += GRID_SCALE){
			gr.drawLine(x, 0, x, this.getHeight());
		}
		for (int y = 0; y < this.getHeight(); y += GRID_SCALE) {
			gr.drawLine(0, y, this.getWidth(), y);
		}
		
	}
	

	/*
	 * Helper method that prints a Labeled Point to this JPanel
	 */
	private Object paintLabeledPoint(Graphics g, LabeledPoint point) {
		Graphics2D gr = (Graphics2D) g;
		gr.drawString(point.getLabel(), point.getX() + 10, point.getY() );
		gr.draw(new RoundRectangle2D.Double(point.getX(),point.getY(), 5, 5, 40, 40));
		return null;
	}
	/**
	 * Changes the background color of this JPanel to the color passed
	 * @param color Color to change the background to.
	 */
	public void setBackgroundColor(Color color){
		this.backgroundColor = color;
	}
	/**
	 * Sets the color of all shapes to be drawn in.
	 * <p>
	 * BEWARE once this method is called all shapes that are on the panel will now be drawn in 
	 * that color.
	 * @param color
	 */
	public void setPenColor(Color color){
		this.penColor = color;
	}
	/**
	 * Constructs a line that will be drawn when repaint is called
	 * @param x1 Starting x value
	 * @param y1 Starting y value 
	 * @param x2 Ending x value
	 * @param y2 Ending y value
	 */
	public void createLine(int x1, int y1, int x2, int y2) {
		Line2D test = new Line2D.Float(x1,y1,x2,y2);
		thingsToDraw.add(test);
	}
	/**
	 * Creates a point that will be drawn to the JPanel when repaint on the parent is called
	 * @param x X coordinate of the point
	 * @param y Y coordinate of the point
	 */
	public void createPoint(int x,int y){
		thingsToDraw.add(new RoundRectangle2D.Double(x,y, 5, 5, 40, 40));
	}
	/**
	 * Creates a Point with a label to the top right of where the point is located at
	 * @param x X coordinate of the point
	 * @param y Y coordinate of the point
	 * @param label Name of the point
	 */
	public void createLabeledPoint(int x, int y, String label){
		
			labeledPoints.add(new LabeledPoint(x,y,label));
		
	}
	
	/**
	 * Clears this JPanel of all contents. Will keep background color.
	 */
	public void clearArea(){
		thingsToDraw.clear();
		labeledPoints.clear();
	}
}
